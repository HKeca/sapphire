#ifndef VGA_H
#define VGA_H

enum {
  COLOR_BLACK   = 0,
  COLOR_BLUE    = 1,
  COLOR_GREEN   = 2,
  COLOR_CYAN    = 3,
  COLOR_RED     = 4,
  COLOR_MAGENTA = 5,
  COLOR_BROWN   = 6,
  COLOR_L_GREY  = 7,
  COLOR_D_GREY  = 8,
  COLOR_L_BLUE  = 9,
  COLOR_L_GREEN = 10,
  COLOR_L_CYAN  = 11,
  COLOR_L_RED   = 12,
  COLOR_L_MAG   = 13,
  COLOR_L_BROWN = 14,
  COLOR_WHITE   = 15
} vga_colors;

#define VGA_CRT_IC 0x3d4 // index port
#define VGA_CRT_DC 0x3d5 // data port

struct point {
  int x;
  int y;
};

struct vga_char {
  char ch: 8;
  char fc: 4;
  char bc: 4;
};

void put_pixel(int x, int y, int color);
void put_rect(unsigned char r, unsigned char g, unsigned char b, unsigned char w,
              unsigned char h);
void vga_clear(void);
void vga_putchar_at(int x, int y, char ch);
void vga_putchar(char ch);
void vga_putstring(const char *str);
void vga_setcur(int x, int y);
void vga_init(void);

struct point vga_getcur(void);

#endif
