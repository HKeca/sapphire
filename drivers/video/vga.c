/*
 * VGA Driver
 */
#include "vga.h"

extern char read_port(unsigned short port);
extern char write_port(unsigned short port, unsigned char data);

// Video setup
struct vga_char *vidptr;
struct vga_char color;
char *vgaptr = 0xA0000;

// Cursor
struct point cur;

// Types
typedef unsigned int uint32_t;
typedef int int32_t;
typedef unsigned short uint16_t;
typedef short int16_t;
typedef unsigned char uint8_t;
typedef char int8_t;

// Memory
void *memcpy(void *dest, const void *src, uint32_t count){
    const uint8_t *sp = (const uint8_t *)src;
    uint8_t *dp = (uint8_t *)dest;

    for(; count != 0; count--) *dp++ = *sp++;
    return dest;
}

uint16_t *memsetw(uint16_t *dest, uint16_t val, uint32_t count){
    uint16_t *temp = (uint16_t *)dest;

    for( ; count != 0; count--) *temp++ = val;
    return dest;
}

/*
 * Move cursor
 */
static void move_cur(void) {
  uint16_t tmp;

  tmp = cur.y * 80 + cur.x;

  write_port(VGA_CRT_IC, 0xe);
  write_port(VGA_CRT_DC, tmp >> 8);

  write_port(VGA_CRT_IC, 0xf);
  write_port(VGA_CRT_DC, tmp);
}

/*
 * Scroll
 */
static void scroll(void) {
  uint16_t blank = color.ch | ((color.bc << 4) | color.fc) << 8;

  if (cur.y >= 25) {
    memcpy(vidptr, vidptr + 80, 80 * 24 * 2);
    memsetw((uint16_t *)vidptr + 80 * 24, blank, 80);
    cur.y--;
  }
}

/*
 * Init
 */
void vga_init(void) {
  cur.x = 0;
  cur.y = 0;
  move_cur();
  
  vidptr = (struct vga_char*)0xb8000;
 
  color.ch = ' ';
  color.fc = COLOR_L_GREY;
  color.bc = COLOR_BLACK;

  put_pixel(2, 3, COLOR_RED);
  put_rect(255, 0, 0, 20, 20);
}

/*
 * Clear screen
 */
void vga_clear(void) {
  int x, y;
  struct vga_char blank = {' ', COLOR_L_GREY, COLOR_BLACK};

  for (y = 0; y < 25; y++) {
    for (x = 0; x < 80; x++) {
      vidptr[y * 80 + x] = blank;
    }
  }

  cur.x = cur.y = 0;
  move_cur();
}

/*
 * print character
 */
void vga_putchar(char ch) {
  switch (ch) {
    case '\r': cur.x = 0; break;
    case '\n': cur.y = cur.y + 1; cur.x = 0; break;
    case '\b':
      cur.y -= (cur.x == 0) ? 1 : 0;
      cur.x = (cur.x + 80 - 1) % 80;
      vidptr[cur.y * 80 + cur.x].ch = ' ';
      vidptr[cur.y * 80 + cur.x].fc = color.fc;
      vidptr[cur.y * 80 + cur.x].bc = color.bc;
      break;
    case '\t': do vga_putchar(' '); while (cur.x % 4 != 0); break;
    default: {
      vidptr[cur.y * 80 + cur.x].ch = ch;
      vidptr[cur.y * 80 + cur.x].fc = color.fc;
      vidptr[cur.y * 80 + cur.x].bc = color.bc;
    
      cur.y += (cur.x + 1) / 80;
      cur.x = (cur.x + 1) % 80;
    }
  }

  scroll();
  move_cur();
}

/*
 * print character at location
 */
void vga_putchar_at(int x, int y, char ch) { 
  vidptr[y * 80 + x].ch = ch;
  vidptr[y * 80 + x].fc = color.fc;
  vidptr[y * 80 + x].bc = color.bc;
}

/*
 * print string
 */
void vga_putstring(const char *str) {
  int i = 0;

  while (str[i] != '\0') {
    vga_putchar(str[i++]);
  }
}

/*
 * Set video color
 */
void vga_setcolor(char fc, char bc) {
  if (fc < 0 || fc > 16 || bc < 0 || bc > 16)
    return;

  color.fc = fc;
  color.bc = bc;
}

/*
 * Set cursor
 */
void vga_setcur(int x, int y) {
  if (x < 0 || x >= 80 || y < 0 || y >= 25)
    return;

  cur.x = x;
  cur.y = y;
}

/*
 * Get cursor
 */
struct point vga_getcur(void) {
  return cur;
}

void put_pixel(int x, int y, int color) {
  unsigned int where = x*25 + y*80;
  
  vgaptr[where] = color & 255;
  vgaptr[where + 1] = (color >> 8) & 255;
  vgaptr[where + 2] = (color >> 16) & 255;
}

void put_rect(unsigned char r, unsigned char g, unsigned char b, unsigned char w,
              unsigned char h) {
  int i, j;

  for (i = 0; i < w; i++) {
    for (j = 0; j < h; j++) {
      vgaptr[j*4] = r;
      vgaptr[j*4 + 1] = g;
      vgaptr[j*4 + 2] = b;
    }
    vgaptr += 80;
  }
}

