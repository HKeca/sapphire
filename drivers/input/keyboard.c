/*
 * Keyboard driver
 */
#include "keyboard.h"
#include "keymap.h"
#include "../../main/screen.h"
#include "../video/vga.h"

#define KEYBOARD_STATUS_PORT 0x64
#define KEYBOARD_DATA_PORT 0x60
#define ENTER_KEY_CODE 0x1C

extern void keyboard_handler(void);
extern char read_port(unsigned short port);
extern char write_port(unsigned short port, unsigned char data);

static unsigned int kb_mode = 0;

/*
 * Keyboard init
 */
void keyboard_init(void) {
  write_port(0x21, 0xFD);
}

void keyboard_handle_input(void) {
  unsigned char status;
  unsigned int released = 0;
  char keycode;

  write_port(0x20, 0x20);

  status = read_port(KEYBOARD_STATUS_PORT);
 
  if (status & 0x01) {
    keycode = read_port(KEYBOARD_DATA_PORT);

    if (keycode == 0x2A || keycode == 0x36) {
      kb_mode |= 0x01;
    }
    
    if (keycode < 0)
      return;

    char output; 
 
    if ((kb_mode & 0x01) != 0) {
      output = keyboard_map_shift[(unsigned char)keycode];
    } else {
      output = keyboard_map[(unsigned char)keycode];
    }

    if (output != 0)
      vga_putchar((const)output);

    kb_mode &= ~0x01;
  }
}   
