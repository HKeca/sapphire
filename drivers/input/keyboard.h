#ifndef KEYBOARD_H
#define KEYBOARD_H
  
void keyboard_init(void);
int set_modifier(char key, unsigned int released);
void keyboard_handle_input(void);  

#endif
