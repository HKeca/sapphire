CFLAGS = -ffreestanding

SRCS = load.asm kernel.c main/gdt.c \
				main/idt.c drivers/input/keyboard.c \
				drivers/video/vga.c
OBJS = $(foreach src,$(SRCS), $(basename $(src)).o)

LINK_SCRIPT = link.ld

KERN_BIN = kernel

all: $(KERN_BIN)

clean: 
	rm $(KERN_BIN) 
	rm $(OBJS)

$(KERN_BIN): $(OBJS)
	ld -m elf_i386 -T $(LINK_SCRIPT) -o $@ $(OBJS)

%.o: %.asm
	nasm -f elf32 $< -o $@

%.o: %.c
	gcc -m32 $(CFLAGS) -c $< -o $@
