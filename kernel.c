/*
* kernel
*/
#include "main/idt.h"
#include "main/gdt.h"

#include "drivers/input/keyboard.h"
#include "drivers/video/vga.h"

void kmain(void)
{
  const char *str = "==== Python sucks ====";
  vga_init();
  vga_clear();
  vga_putstring(str);
 
  gdt_init();
  idt_init();
  keyboard_init();

  while(1);
}
