;; Kernel
bits 32
section .text
  align 4
  dd 0x1BADB002
  dd 0x00
  dd - (0x1BADB002 + 0x00)

global start
global keyboard_handler
global read_port
global write_port
global load_idt
global gdt_flush

extern kmain
extern keyboard_handle_input
extern gp

read_port:
  mov edx, [esp + 4]
  in al, dx
  ret

write_port:
  mov edx, [esp + 4]
  mov al, [esp + 4 + 4]
  out dx, al
  ret

load_idt:
  mov edx, [esp + 4]
  lidt [edx]
  sti
  ret

gdt_flush:
  lgdt [gp]
  mov ax, 0x10
  mov ds, ax
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax
  jmp 0x08:flush2

flush2:
  ret

keyboard_handler:
  call keyboard_handle_input
  iretd

start:
  cli
  mov esp, stack_space
  call kmain
  hlt

section .bss
resb 8192
stack_space:

