/*
 * Screen handler
 */
#include "screen.h"

// Screen settings
#define LINES 25
#define COLUMNS_IN_LINE 80
#define BYTES_FOR_EACH_ELEMENT 2
#define SCREEN_SIZE BYTES_FOR_EACH_ELEMENT * COLUMNS_IN_LINE * LINES

// Current cursor location
unsigned int current_loc = 0;
// Video memory
char *vidptr = (char*)0xb8000;

/*
 * Screen
 */
void screen_init(void) {
  screen_clear();
}

/*
 * Clear screen
 */
void screen_clear(void) {
  unsigned int i = 0;

  while (i < SCREEN_SIZE) {
    vidptr[i++] = ' ';
    vidptr[i++] = 0x02;
  }
  
  current_loc = 0;
}

/*
 * Screen newline
 */
void screen_newline(void) {
  unsigned int line_size = BYTES_FOR_EACH_ELEMENT * COLUMNS_IN_LINE;
  current_loc = current_loc + (line_size - current_loc % (line_size));
}

/*
 * Screen print
 */
void screen_print(const char *str) {
  unsigned int i = 0;
  while (str[i] != '\0') {
    vidptr[current_loc++] = str[i++];
    vidptr[current_loc++] = 0x02;
  }
}

// Not sure if this is safe?
void screen_print_single(const char str) {
  vidptr[current_loc++] = str;
  vidptr[current_loc++] = 0x02;
}

// backspace
void screen_delete_last_letter(void) {
  vidptr[current_loc--] = ' ';
  vidptr[current_loc--] = 0x02;
}
