#ifndef SCREEN_H
#define SCREEN_H

void screen_init(void);
void screen_clear(void);
void screen_newline(void);
void screen_print(const char *str);
void screen_print_single(const char str);
void screen_delete_last_letter(void);

#endif
