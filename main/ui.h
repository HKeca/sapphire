#ifndef UI_H
#define UI_H

typedef unsigned short uint16_t;

typedef struct tagBITMAP {
  unsigned int width;
  unsigned int height;
  unsigned char *data;
} BITMAP;

typedef struct tabRECT {
  long x1;
  long y1;
  long x2;
  long y2;
} RECT;

void setpixel(BITMAP *bmp, int x, int y, unsigned char color);
void drawrect(BITMAP *bmp, uint16_t x, uint16_t y, uint16_t x2, uint16_t y2, unsigned char color);

#endif
