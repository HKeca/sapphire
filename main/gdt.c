#include "gdt.h"

struct gdt_entry gdt[3];
struct gdt_ptr gp;

void gdt_set_gate(int index, unsigned long base, unsigned long limit,
                  unsigned char access, unsigned char gran) {
  // Descriptor base
  gdt[index].base_low = (base & 0xFFFF);
  gdt[index].base_middle = (base >> 16) & 0xFF;
  gdt[index].base_high = (base >> 24) & 0xFF;

  // Descriptor limits
  gdt[index].limit_low = (limit & 0xFFFF);
  gdt[index].gran = ((limit >> 16) & 0x0F);

  gdt[index].gran |= (gran & 0xF0);
  gdt[index].access = access;
}

void gdt_init(void) {
  gp.limit = (sizeof(struct gdt_entry) * 3) - 1;
  gp.base = (unsigned int)&gdt;

  gdt_set_gate(0, 0, 0, 0, 0);

  gdt_set_gate(1, 0, 0xFFFFFFFF, 0x9A, 0xCF);

  gdt_set_gate(2, 0, 0xFFFFFFFF, 0x92, 0xCF);

  gdt_flush();
}
