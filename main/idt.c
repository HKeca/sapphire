#include "idt.h"

#define IDT_SIZE 256
#define INTERRUPT_GATE 0x8e
#define KERNEL_CODE_SEGMENT_OFFSET 0x08

extern void keyboard_handler(void);
extern char read_port(unsigned short port);
extern void write_port(unsigned short port, unsigned char data);
extern void load_idt(unsigned long *idt_ptr);

struct IDT_entry IDT[IDT_SIZE];

void idt_init(void) {
  unsigned long keyboard_address;
  unsigned long idt_address;
  unsigned long idt_ptr[2];

  keyboard_address = (unsigned long)keyboard_handler;
  IDT[0x21].offset_lowerbits = keyboard_address & 0xffff;
  IDT[0x21].selector = KERNEL_CODE_SEGMENT_OFFSET;
  IDT[0x21].zero = 0;
  IDT[0x21].type_attr = INTERRUPT_GATE;
  IDT[0x21].offset_higherbits = (keyboard_address & 0xffff0000) >> 16;

  /*
   * Ports
   *          PIC1  PIC2
   * Command  0x20  0xA0
   * Data     0x21  0xA1
   * 
   */
  
  /* ICW 1 */
  write_port(0x20, 0x11);
  write_port(0xA0, 0x11);

  /* ICW2 remap offset */ 
  write_port(0x21, 0x20);
  write_port(0xA1, 0x28);
  
  /* ICW3 */
  write_port(0x21, 0x00);
  write_port(0xA1, 0x00);

  /* ICW4 */
  write_port(0x21, 0x01);  
  write_port(0xA1, 0x01);

  /* mask interrupts */
  write_port(0x21, 0xff);   
  write_port(0xA1, 0xff); 

  /* fill the IDT descriptor */
  idt_address = (unsigned long)IDT;
  idt_ptr[0] = (sizeof (struct IDT_entry) * IDT_SIZE) + ((idt_address & 0xffff) << 16);
  idt_ptr[1] = idt_address >> 16;

  load_idt(idt_ptr);
}
